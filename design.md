- Create vertical buffer with same number of lines
- Scrollbind, cursorbind
- When adding new line in source file, add new line in notes buffer
- When deleting line, defer / delete & keep history stack
- When overflowing in notes, find a way to keep in sync If we add new lines (hard wrap),
  how to keep sync? Keep an offset for original file?  If we soft wrap, make sure
  scrollbind works (it doesn't with wraps currently)

## Architecture of plugin

Let source file `src`. Copy `src` to internal copy `src-view` and map `src` to `src-view`.
Create notes file `notes` that is mapped to `src`.

## Handling multiple note lines per source line

**crb and wrap don't like each other!**

Set hard wrap on notes buffer, and count lines before and after insert to see how many
extra lines were inserted.

> What to do with the extra lines inserted? Maybe have a custom:
	- Moving script (aka, mapping between src-notes lines)
	- Notes line numbers (easier, but not visual)

Add extra lines to src file (which is a copy of the source).

## Merging feature

Special comment to support merging with source file, e.g.

// #SNHEAD A: Dae
// this is a line that explains
// #SNEND


