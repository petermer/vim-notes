if exists('g:loaded_sidenotes') || &cp
	finish
endif
let g:loaded_sidenotes = 1

let s:note_extension = 'sidenote'
let s:source_extension = 'sidesrc'
let s:magic_identifier = '$SN::ANN$'

let s:cpo_save = &cpo
set cpo&vim

function! s:mapping_exists(name)
	if !has_key(s:sidenote_entries, a:name)
		echom 'No sidenote map for file ' . a:name
	endif
endfunction

function! sidenote#get_parent(item) abort
	if !has_key(s:parents, a:item)
		echoe 'There is no child-parent relationship for ' . a:item
	endif

	return s:parents[a:item]
endfunction

function! sidenote#is_notes_buf(name) abort
	return a:name =~? '\.' . s:note_extension . '$'
endfunction

" Detect if positioned over annotation block
function! sidenote#is_in_annotation() abort
	let notesBufName = expand('%:p')

	if sidenote#is_notes_buf(notesBufName) == 0
		echoe 'Not a notes buffer!'
		return
	endif

	return !empty(getline(line('.')))
endfunction

function! sidenote#prepare_insert() abort
	let notesBufName = expand('%:p')
	let entryKey = sidenote#get_parent(notesBufName)
	let s:sidenote_entries[entryKey]['last_insert'] = line('.')
endfunction

function! sidenote#after_insert() abort
	let after_lcount = line('$')
	let notesBufName = expand('%:p')
	let entryKey = sidenote#get_parent(notesBufName)
	let pre_count = s:sidenote_entries[entryKey]['lines']

	if pre_count < after_lcount
		call sidenote#pad_source_view(entryKey, after_lcount - pre_count)
		let s:sidenote_entries[entryKey]['lines'] = after_lcount

		" set a fold over the annotation area
		let foldStart = s:sidenote_entries[entryKey]['last_insert']
		exec foldStart . ',' . (foldStart + after_lcount - pre_count) . 'fold'
	endif
endfunction

function! sidenote#pad_source_view(entry, extraLines)
	let srcViewBufName = s:sidenote_entries[a:entry]['view']
	let notesBufName = s:sidenote_entries[a:entry]['notes']
	exec bufwinnr(srcViewBufName) . 'wincmd w'
	call append(s:sidenote_entries[a:entry]['last_insert'], map(range(a:extraLines), 's:magic_identifier'))
	exec bufwinnr(notesBufName) . 'wincmd w'
endfunction

function! sidenote#leave_notes() abort
	" enable folds
	setlocal foldenable
	setlocal nonumber
endfunction

function! sidenote#switch_to_source() abort
	" In here we're already switched to the src-view buffer
	" Attempt to persist cursor position
	let saveLinePos = line('.')
	let srcViewBufName = expand('%:p')
	let entryKey = sidenote#get_parent(srcViewBufName)

	" Let the custom scrolling ensue...
	let saveWinOffset = saveLinePos - winline() + &scrolloff + 1

	" Will this work lol
	exec 'b ' . entryKey
	exec 'normal! ' . saveWinOffset . "z\<cr>"
	exec 'normal! ' . saveLinePos . 'G'
	setlocal cursorbind scb
endfunction

function! sidenote#switch_to_notes() abort
	" In here we're already switched to the notes buffer
	let notesBufName = expand('%:p')
	let entryKey = sidenote#get_parent(notesBufName)

	setlocal nofoldenable
	setlocal number

	exec bufwinnr(entryKey) . 'wincmd w'
	exec 'b ' . s:sidenote_entries[entryKey]['view']
	exec bufwinnr(notesBufName) . 'wincmd w'
	syncbind
endfunction

function! sidenote#after_notes_saved() abort
	" Attempt to write view file
	let notesBufName = expand('%:p')
	let entryKey = sidenote#get_parent(notesBufName)
	let srcViewBufName = s:sidenote_entries[entryKey]['view']

	" Save folds view for current notes buffer
	silent! mkview

	exec 'noautocmd ' . bufwinnr(srcViewBufName) . 'wincmd w'
	update
	exec 'noautocmd ' . bufwinnr(notesBufName) . 'wincmd w'
endfunction

function! sidenote#notes_buffer_name(full_path)
	return s:default_file_dir() . fnamemodify(a:full_path, ':t:r') . '.' . s:note_extension
endfunction

function! sidenote#view_buffer_name(full_path)
	return s:default_file_dir() . fnamemodify(a:full_path, ':t:r') . '.' . s:source_extension
endfunction

function! s:create_mappings(key, opt)
	" Associate notes view to original source
	if !exists('s:sidenote_entries')
		let s:sidenote_entries = {}
	endif
	let s:sidenote_entries[a:key] = {
		\ 'view':			a:opt['view'],
		\ 'notes':			a:opt['notes'],
		\ 'lines':			a:opt['lines'],
		\ 'last_insert':	0,
	\ }
endfunction

" FIXME: Only undo src-view buffer if extra lines added!
function! sidenote#undo()
	let notesBufName = expand('%:p')
	let entryKey = sidenote#get_parent(notesBufName)
	let srcViewBufName = s:sidenote_entries[entryKey]['view']

	exec join(sort([bufwinnr(notesBufName), bufwinnr(srcViewBufName)]), ',') . 'windo undo'

	exec bufwinnr(notesBufName) . 'wincmd w'
	let s:sidenote_entries[entryKey]['lines'] = line('$')
endfunction

function! s:default_file_dir() abort
	let path = getcwd(). '/.vim-sidenotes/'
	if !isdirectory(path)
		call mkdir(path, 'p')
	endif
	return path
endfunction

function! sidenote#foldtext() abort
	let line = getline(v:foldstart)
	return line
endfunction

function! s:create_notes()
	let lcount = line('$')
	let srcFileName = expand('%:p')
	let srcFileNameRoot = expand('%:p:r')
	let saveLinePos = line('.')
	let viewBufName = sidenote#view_buffer_name(srcFileName)

	" Disable wrapping for original file
	setlocal nowrap

	" Create a temp source file view
	let srcFT = &ft
	exec 'e ' . viewBufName
	let &ft = srcFT
	if !filereadable(viewBufName)
		exec '0r ' . srcFileName
		exec line('$') . 'd'
		w
	endif

	" Create a notes file
	exec 'vnew ' . sidenote#notes_buffer_name(srcFileName)
	setlocal fdm=manual
	setlocal nofoldenable
	setlocal foldtext=sidenote#foldtext()
	setlocal viewoptions=folds
	setlocal fillchars-=fold:-
	setlocal fillchars+=fold:.

	" -- EXPERIMENTAL: Load (folds) view ---
	" This will probably fail when encountering
	" a previously removed sidenote file.
	silent! loadview

	if line('$') < lcount
		let extras = lcount - line('$')
		call setline(line('$') + 1, map(range(extras), '""'))
	elseif line('$') > lcount
		" need to chop off lines?
	endif
	let lcount = line('$') " this is the line count for the src-view

	exec 'normal! 1G'
	exec 'vertical res 50'
	setlocal scb
	setlocal crb
	setlocal nowrap
	" setlocal nonumber " no need to complicate things
	setlocal tw=40

	let splitWinNum = bufwinnr(sidenote#notes_buffer_name(srcFileName))
	let srcWinNum = bufwinnr(viewBufName)
	let entryKey = fnamemodify(srcFileName, ':p:.')

	call s:create_mappings(entryKey, {
		\ 'view': viewBufName,
		\ 'notes': sidenote#notes_buffer_name(srcFileName),
		\ 'lines': lcount
	\ })

	let s:parents = {}
	let s:parents[viewBufName] = entryKey
	let s:parents[sidenote#notes_buffer_name(srcFileName)] = entryKey

	" Map insert mode hacks
	nnoremap <buffer> <silent> o		A<cr>
	nnoremap <buffer> <silent> O 		:echom 'Inserting above is unimplemented/not needed'<cr>
	nnoremap <buffer> <silent> u 		:echom 'Undo unimplemented...'<cr>
	" nnoremap <buffer> <silent> u 		:SNUndo<cr>
	nnoremap <buffer> <silent> <C-R>	<nop>

	exec srcWinNum . 'wincmd w'
	exec 'normal! 1G'
	setlocal scb
	setlocal crb
	setlocal nowrap
	exec 'normal! ' . saveLinePos . 'G'

	exec splitWinNum . 'wincmd w'
	exec 'normal! ' . saveLinePos . 'G'

	" define autocommands
	augroup sidenote_management
		au!
		exec 'au InsertEnter *.' . s:note_extension . ' call sidenote#prepare_insert()'
		exec 'au InsertLeave *.' . s:note_extension . ' call sidenote#after_insert()'
		exec 'au BufEnter *.' . s:source_extension . ' call sidenote#switch_to_source()'
		exec 'au BufLeave *.' . s:note_extension . ' call sidenote#leave_notes()'
		exec 'au BufEnter *.' . s:note_extension . ' call sidenote#switch_to_notes()'

		" When saving the notes file, save the .sidesrc buffer
		exec 'au BufWritePost,FileWritePost *.' . s:note_extension . ' call sidenote#after_notes_saved()'
	augroup END
endfunction

function! s:try_load_notes(fname)
	if filereadable(a:fname)
		let in_note_section = 0
		for line in readfile(a:fname, '', 10)
			if match(line, '// #SNHEAD') != -1
				let in_note_section = 1
			elseif in_note_section == 1 && match(line, '// #SNEND') != -1
				let in_note_section = 0
			elseif in_note_section == 1
				echom line
			end
		endfor
	endif
endfunction

command! -nargs=0 NAnnotate call s:create_notes()
command! -nargs=0 SNLoad call s:try_load_notes(expand('%:p'))
command! -nargs=0 SNUndo call sidenote#undo()

let &cpo = s:cpo_save
